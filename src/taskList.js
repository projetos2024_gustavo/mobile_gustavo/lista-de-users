import React from "react";
import { View, Text, TouchableOpacity, FlatList, Image, StyleSheet } from "react-native";

const TaskList = ({ navigation }) => {
  const tasks = [
    {
      id: 1,
      title: "Ir ao mercado",
      date: "2024-02-27",
      time: "10:00",
      address: "Supermercado SS",
      image: require('../../assets/mercado.jpg')
    },
    {
      id: 2,
      title: "Enviar e-mail",
      date: "2024-02-28",
      time: "14:30",
      address: "Trabalho",
      image: require('../../assets/email.png')
    },
    {
      id: 3,
      title: "Estudar React Native",
      date: "2024-03-01",
      time: "09:00",
      address: "Casa",
      image: require('../../assets/React.png')
    },
  ];

  const taskPress = (task) => {
    navigation.navigate("DetalhesDasTarefas", { task });
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={tasks}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => taskPress(item)}>
            <View style={styles.taskItem}>
              <Text style={styles.title}>{item.title}</Text>
              <Image
                source={item.image}
                style={styles.image}
              />
            </View>
          </TouchableOpacity>
        )}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
    paddingTop: 16,
  },
  taskItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginRight: 10,
  },
  image: {
    width: 50,
    height: 50,
    resizeMode: 'cover',
  },
  separator: {
    height: 1,
    backgroundColor: '#CCCCCC',
  },
});

export default TaskList;
