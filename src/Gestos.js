import React from "react";
import { View, PanResponder, StyleSheet, Text, Dimensions } from "react-native";

const Gestos = ({ navigation }) => {
  const screenWidth = Dimensions.get('window').width;
  const panResponder = React.useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gestureState) => {
        console.log("Movimento X:", gestureState.dx);
        console.log("Movimento Y:", gestureState.dy);
      },
      onPanResponderRelease: (event, gestureState) => {
        if (gestureState.dx > screenWidth / 2) {
          navigation.goBack();
        }
      },
    })
  ).current;
  return (
    <View style={styles.container} {...panResponder.panHandlers}>
      <Text style={styles.text}>OI</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#ffff",
  },
  text: {
    fontSize: 20,
    fontWeight: "bold",
  },
});
export default Gestos;
